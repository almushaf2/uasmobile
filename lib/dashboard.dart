import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dashboard',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: DashboardScreen(),
    );
  }
}

class DashboardScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dashboard'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            child: Row(
              children: [
                Expanded(
                  child: DashboardBox(
                    title: 'Profil',
                    color: Colors.orange,
                    icon: Icons.person,
                  ),
                ),
                Expanded(
                  child: DashboardBox(
                    title: 'Cari',
                    color: Colors.green,
                    icon: Icons.search,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: DashboardBox(
              title: 'Tanggapan',
              color: Colors.blue,
              icon: Icons.comment,
            ),
          ),
          SizedBox(height: 16.0),
          DashboardBox(
            title: 'Masukkan Gambar',
            color: Colors.purple,
            icon: Icons.camera_alt,
          ),
        ],
      ),
    );
  }
}

class DashboardBox extends StatelessWidget {
  final String title;
  final Color color;
  final IconData icon;

  const DashboardBox({
    Key? key,
    required this.title,
    required this.color,
    required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(16.0),
      padding: EdgeInsets.all(16.0),
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            icon,
            size: 48,
            color: Colors.white,
          ),
          SizedBox(height: 16.0),
          Text(
            title,
            style: TextStyle(
              fontSize: 18,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
