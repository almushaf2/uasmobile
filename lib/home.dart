import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_uts/cancer.dart';
import 'package:flutter_uts/login.dart';

class HomePage extends StatefulWidget {
  void logOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Pilih Donasi"),
        ),
        body: Container(
          decoration: BoxDecoration(
            color: Colors.grey[200], // Warna latar belakang
            borderRadius:
                BorderRadius.circular(10.0), // Membuat sudut border melengkung
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.5),
                spreadRadius: 5,
                blurRadius: 7,
                offset: Offset(0, 3), // Atur bayangan di bawah kontainer
              ),
            ],
          ),
          padding: EdgeInsets.all(16.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Children Cancer Hospital '),
                Image.network(
                    'https://unityeducationhub.com/wp-content/uploads/2021/03/QQ%E6%88%AA%E5%9B%BE20210326074533.png',
                    height: 150),
                Text('Food Scarcity Donation'),
                Image.network(
                  'https://c.ndtvimg.com/2020-01/79c3o6og_food-scarcity-malnutrition_625x300_27_January_20.jpg',
                  height: 150,
                  width: 900,
                ),
                Text('War Refugee Donations'),
                Image.network(
                    'https://th.bing.com/th/id/OIP.GaK6_a9hSLnWvqTJTb9ZqQAAAA?pid=ImgDet&rs=1',
                    height: 150),

                SizedBox(height: 20), // Jarak antara gambar dan tombol kembali

                InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Cancer(),
                      ),
                    );
                  },
                  child: Text(
                    'Donate',
                    style: TextStyle(
                      color: Colors.blue,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ),
                SizedBox(height: 20),
                ElevatedButton.icon(
                  style: ButtonStyle(
                      backgroundColor: MaterialStatePropertyAll(Colors.red)),
                  onPressed: logOut,
                  icon: Icon(Icons.logout_outlined),
                  label: Text("Logout!"),
                ),
              ],
            ),
          ),
        ));
  }

  void logOut() async {
    await FirebaseAuth.instance.signOut();
  }
}

//Text(" Menu Utama"),
//            ElevatedButton.icon(
//              style: ButtonStyle(
  //                backgroundColor: MaterialStatePropertyAll(Colors.red)),
    //          onPressed: logOut,
      //        icon: Icon(Icons.logout_outlined),
        //      label: Text("Logout!"),
          //  ),