import 'package:flutter/material.dart';
import 'package:flutter_uts/informasi.dart';

void main() {
  runApp(Cancer());
}

class Cancer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Children Cancer Hospital',
      home: ChildrenCancerHospitalPage(),
    );
  }
}

class ChildrenCancerHospitalPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Children Cancer Hospital'),
      ),
      backgroundColor: Color(0xFFf5f5dc),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.network(
                'https://th.bing.com/th/id/OIP.Ga5NbGtwOTISTXw54ZyFCQHaE8?pid=ImgDet&rs=1',
                height: 200),
            SizedBox(height: 20),
            Text(
              'Children Cancer Hospital Donation',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Text(
                'Children Cancer Hospital adalah sebuah rumah sakit yang khusus menangani pasien anak yang menderita kanker. '
                'Donasi yang Anda berikan akan digunakan untuk biaya perawatan, pendidikan, dan dukungan bagi anak-anak penderita kanker '
                'serta penelitian untuk menemukan pengobatan yang lebih baik.',
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => InformationForm()),
                );
              },
              child: Text('Donasi Sekarang'),
            ),
          ],
        ),
      ),
    );
  }
}
