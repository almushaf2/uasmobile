import 'package:flutter/material.dart';
import 'package:flutter_uts/informasi.dart';

class Pembayaran extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Payment Portal'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: 16),
            Text('Cash'),
            Image.network(
              'https://freepngimg.com/thumb/money/76121-united-banknote-money-photography-dollar-cash-handheld.png',
              width: 100,
              height: 100,
            ),
            Divider(
              color: Colors.black,
              thickness: 1,
              height: 24,
              indent: 16,
              endIndent: 16,
            ),
            SizedBox(height: 32),
            Image.network(
              'https://th.bing.com/th/id/OIP.nqNybSxjt8PCelBq1TaAXAHaE-?pid=ImgDet&rs=1',
              width: 100,
              height: 100,
            ),
            SizedBox(height: 16),
            Text('Credit Card / Debit Card Payment'),
          ],
        ),
      ),
    );
  }
}
